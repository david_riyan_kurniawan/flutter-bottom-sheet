import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

//membuat class baru bernama HomePage
class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bottom sheet'),
      ),
      body: Center(
        child: ElevatedButton(
            onPressed: () {
              showModalBottomSheet(
                backgroundColor: Colors.blue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                context: context,
                builder: (context) => SizedBox(
                  height: 300,
                  child: ListView(
                    children: [
                      ListTile(
                        leading: Icon(
                          Icons.photo,
                        ),
                        title: Text('Photo'),
                      ),
                      ListTile(
                        leading: Icon(
                          Icons.photo,
                        ),
                        title: Text('Photo'),
                      ),
                      ListTile(
                        leading: Icon(
                          Icons.photo,
                        ),
                        title: Text('Photo'),
                      )
                    ],
                  ),
                ),
              );
            },
            child: Text('BOTTOM SHEET')),
      ),
    );
  }
}
